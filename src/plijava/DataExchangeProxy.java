/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::plis
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

package plijava;

import Ice.Communicator;
import Ice.Current;
import Ice.ObjectPrx;

import mseg.DataExchangeInterfacePrx;
import mseg.DataExchangeInterfacePrxHelper;
import mseg.DataHandlingException;

public class DataExchangeProxy
{
    private DataExchangeInterfacePrx data;

    public DataExchangeProxy()
    {
        
    }

    public void connect(Communicator ic)
    {
        ObjectPrx de = ic.stringToProxy("DataExchange");
        this.data = DataExchangeInterfacePrxHelper.checkedCast(de);
    }

    public int getFrameCount()
    {
        return this.data.getFrameCount(null);
    }

    public String getMMMFile()
    {
        return this.data.getMMMFile(null);
    }

    public String getViconFile()
    {
        return this.data.getViconFile(null);
    }

    public int[] getGroundTruth()
    {
        return this.data.getGroundTruth(null);
    }

    public float[] getJointAnglesForFrame(int n) throws DataHandlingException
    {
        return this.data.getJointAnglesForFrame(n, null);
    }

    public void reportKeyFrame(int frame) throws DataHandlingException
    {
        this.data.reportKeyFrame(frame, null);
    }

    public void reportKeyFrames(int[] frames) throws DataHandlingException
    {
        this.data.reportKeyFrames(frames, null);
    }
}
