/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::plis
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

package plijava;

import java.util.concurrent.atomic.AtomicBoolean;

import Ice.Communicator;
import Ice.Identity;
import Ice.ObjectAdapter;
import Ice.ObjectPrx;
import Ice.ConnectionRefusedException;
import Ice.NotRegisteredException;

import IceGrid.AdminPrx;
import IceGrid.AdminSessionPrx;
import IceGrid.DeploymentException;
import IceGrid.ObjectExistsException;
import IceGrid.ObjectNotRegisteredException;
import IceGrid.PermissionDeniedException;
import IceGrid.RegistryPrx;
import IceGrid.RegistryPrxHelper;

public class AlgorithmServer
{
    public void serve(SegmentationAlgorithm algorithm) throws DeploymentException, PermissionDeniedException
    {
        final AtomicBoolean serverShutDown = new AtomicBoolean(false);

        String home = System.getProperty("user.home");
        String[] parameters = new String[] { "--Ice.Config=" + home + "/.armarx/default.cfg" };

        final Communicator ic = Ice.Util.initialize(parameters);

        // Add this algorithm as object to IceGrid
        ObjectAdapter adapter = ic.createObjectAdapterWithEndpoints(algorithm.internal_getName(), "tcp");
        adapter.activate();

        final Identity id = ic.stringToIdentity(algorithm.internal_getName());
        adapter.add(algorithm, id);
        
        ObjectPrx proxy = adapter.createProxy(id);

        // Try to connect to ArmarX' IceGrid
        try
        {
            RegistryPrx reg = RegistryPrxHelper.checkedCast(ic.stringToProxy("IceGrid/Registry"));

            AdminSessionPrx adminSession = reg.createAdminSession("user", "password");
            AdminPrx admin = adminSession.getAdmin();

            // Try to add algorithm to ArmarX' IceGrid
            try
            {
                admin.addObjectWithType(proxy, proxy.ice_id());
            }
            // Retry to add algorithm if it already exists
            catch (ObjectExistsException e)
            {
                // If adding the object fails because of the exception above, there is a stray object from a previous instance...
                System.out.println("Algorithm with the same name already registered. Perhaps a previous instance crashed. Cleaning up now...");

                try
                {
                    // ...therefore the stray object will be removed...
                    admin.removeObject(id);
                }
                catch (ObjectNotRegisteredException e2) {}

                System.out.println("Retrying...");

                try
                {
                    // ...and the addition of the object to ArmarX' IceGrid will be retried
                    admin.addObjectWithType(proxy, proxy.ice_id());
                }
                catch (ObjectExistsException e2) {}
            }

            // Close session
            adminSession.destroy();

            // Try to connect to DataExchange
            try
            {
                System.out.println("Connecting to MSeg core module...");

                algorithm.getData().connect(ic);

                System.out.println("Connection established");

                // Indicate that initialization finished successfully and waiting for instructions
                System.out.println(algorithm.internal_getName() + " ready");

                Runtime.getRuntime().addShutdownHook(new Thread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ic.shutdown();

                        System.out.println("");

                        // Wait until everything properly shut down
                        while(!serverShutDown.get());
                    }
                }));

                ic.waitForShutdown();
            }
            // Indicate that user has to start mseg if not registered
            catch (NotRegisteredException e)
            {
                System.out.println("MSeg core module not available. Start it by running `msegcm start`");
            }

            System.out.println("Cleaning up...");

            try
            {
                adminSession = reg.createAdminSession("user", "password");
                admin = adminSession.getAdmin();

                admin.removeObject(id);

                adminSession.destroy();
            }
            catch (ObjectNotRegisteredException e) {}
        }
        // Indicate that user has to start armarx
        catch (ConnectionRefusedException e)
        {
            System.out.println("ArmarX is not available. Start it by running 'armarx start'");
        }

        if (ic != null)
        {
            ic.destroy();
        }

        serverShutDown.set(true);
    }
}