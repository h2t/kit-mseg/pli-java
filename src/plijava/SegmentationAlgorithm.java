/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::plis
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

package plijava;

import Ice.Current;

import org.json.JSONArray;
import org.json.JSONObject;

import mseg._SegmentationAlgorithmInterfaceDisp;
import mseg.Granularity;
import mseg.MSegException;
import mseg.DataHandlingException;

public abstract class SegmentationAlgorithm extends _SegmentationAlgorithmInterfaceDisp
{
    private JSONObject parameters;

    protected DataExchangeProxy data;
    protected String name;
    protected boolean requiresTraining;
    protected Granularity trainingGranularity;

    public SegmentationAlgorithm()
    {
        this.parameters = new JSONObject();
        this.parameters.put("parameters", new JSONArray());

        this.data = new DataExchangeProxy();
        this.name = "";
        this.requiresTraining = false;
        this.trainingGranularity = Granularity.Medium;
    }

    public DataExchangeProxy getData()
    {
        return this.data;
    }

    public void train() throws MSegException
    {

    }

    public void resetTraining() throws MSegException
    {
        
    }

    public abstract void segment() throws MSegException;

    @Override
    public boolean internal_requiresTraining(Current __current)
    {
        return this.requiresTraining;
    }

    @Override
    public Granularity internal_getTrainingGranularity(Current __current)
    {
        return this.trainingGranularity;
    }

    @Override
    public void internal_resetTraining(Current __current) throws MSegException
    {
        try
        {
            this.resetTraining();
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    @Override
    public void internal_train(Current __current) throws MSegException
    {
        try
        {
            this.train();
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    @Override
    public void internal_segment(Current __current) throws MSegException
    {
        try
        {
            this.segment();
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    @Override
    public String internal_getName(Current __current)
    {
        return this.name;
    }

    @Override
    public final String internal_getParameters(Current __current)
    {
        return this.parameters.toString();
    }

    @Override
    public final void internal_setParameters(String parameters, Current __current)
    {
        JSONObject newParameters = new JSONObject(parameters);

        for (int i = 0; i < newParameters.getJSONArray("parameters").length(); i++)
        {
            JSONObject newValue = newParameters.getJSONArray("parameters").getJSONObject(i);
            int index = this.findParameterByName(newValue.getString("name"));
            String value = newValue.getString("value");

            this.parameters.getJSONArray("parameters").getJSONObject(index).put("value", value);
        }
    }

    public boolean isParameterRegistered(String name)
    {
        return (this.findParameterByName(name) >= 0);
    }

    public void setParameterValue(String name, String value)
    {
        int index;

        if ((index = this.findParameterByName(name)) < 0)
        {
            throw new RuntimeException("Unknown parameter '" + name + "'");
        }

        this.parameters.getJSONArray("parameters").getJSONObject(index).put("value", value);
    }

    public int findParameterByName(String name)
    {
        int paramCount = this.parameters.getJSONArray("parameters").length();

        for (int i = 0; i < (int) paramCount; i++)
        {
            if (this.parameters.getJSONArray("parameters").getJSONObject(i) .getString("name").equals(name))
            {
                return i;
            }
        }

        return -1;
    }

    public void registerIntParameter(String name, String description, int defaultValue, int min, int max)
    {
        if (this.isParameterRegistered(name))
        {
            throw new RuntimeException("Parameter with the name '" + name + "' already registered");
        }

        JSONObject parameter = new JSONObject();

        parameter.put("name", name);
        parameter.put("description", description);
        parameter.put("type", "int");
        parameter.put("defaultValue", defaultValue + "");
        parameter.put("intmin", min);
        parameter.put("intmax", max);

        this.parameters.getJSONArray("parameters").put(parameter);
    }

    public void registerIntParameter(String name, String description, int defaultValue)
    {
        registerIntParameter(name, description, defaultValue, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    public void registerIntParameter(String name, String description)
    {
        registerIntParameter(name, description, 0);
    }

    public void registerIntParameter(String name)
    {
        registerIntParameter(name, "");
    }

    public void registerFloatParameter(String name, String description, double defaultValue, int decimals, double min, double max)
    {
        if (this.isParameterRegistered(name))
        {
            throw new RuntimeException("Parameter with the name '" + name + "' already registered");
        }

        JSONObject parameter = new JSONObject();

        parameter.put("name", name);
        parameter.put("description", description);
        parameter.put("type", "float");
        parameter.put("defaultValue", defaultValue + "");
        parameter.put("decimals", decimals);
        parameter.put("floatmin", min);
        parameter.put("floatmax", max);

        this.parameters.getJSONArray("parameters").put(parameter);
    }

    public void registerFloatParameter(String name, String description, double defaultValue, int decimals)
    {
        registerFloatParameter(name, description, defaultValue, decimals, -Double.MAX_VALUE, Double.MAX_VALUE);
    }

    public void registerFloatParameter(String name, String description, double defaultValue)
    {
        registerFloatParameter(name, description, defaultValue, 2);
    }

    public void registerFloatParameter(String name, String description)
    {
        registerFloatParameter(name, description, 0);
    }

    public void registerFloatParameter(String name)
    {
        registerFloatParameter(name, "");
    }

    public void registerBoolParameter(String name, String description, boolean defaultValue)
    {
        if (this.isParameterRegistered(name))
        {
            throw new RuntimeException("Parameter with the name '" + name
                    + "' already registered");
        }

        JSONObject parameter = new JSONObject();

        parameter.put("name", name);
        parameter.put("description", description);
        parameter.put("type", "bool");
        parameter.put("defaultValue", defaultValue ? "true" : "false");

        this.parameters.getJSONArray("parameters").put(parameter);
    }

    public void registerBoolParameter(String name, String description)
    {
        registerBoolParameter(name, description, true);
    }

    public void registerBoolParameter(String name)
    {
        registerBoolParameter(name, "");
    }

    public void registerStringParameter(String name, String description, String defaultValue)
    {
        if (this.isParameterRegistered(name))
        {
            throw new RuntimeException("Parameter with the name '" + name
                    + "' already registered");
        }

        JSONObject parameter = new JSONObject();

        parameter.put("name", name);
        parameter.put("description", description);
        parameter.put("type", "string");
        parameter.put("defaultValue", defaultValue + "");

        this.parameters.getJSONArray("parameters").put(parameter);
    }

    public void registerStringParameter(String name, String description)
    {
        registerStringParameter(name, description, "");
    }

    public void registerStringParameter(String name)
    {
        registerStringParameter(name, "");
    }

    public void registerJsonParameter(String name, String description, JSONObject defaultValue)
    {
        if (this.isParameterRegistered(name))
        {
            throw new RuntimeException("Parameter with the name '" + name
                    + "' already registered");
        }

        JSONObject parameter = new JSONObject();

        parameter.put("name", name);
        parameter.put("description", description);
        parameter.put("type", "json");
        parameter.put("defaultValue", defaultValue.toString());

        this.parameters.getJSONArray("parameters").put(parameter);
    }

    public void registerJsonParameter(String name, String description)
    {
        registerJsonParameter(name, description, new JSONObject());
    }

    public void registerJsonParameter(String name)
    {
        registerJsonParameter(name, "");
    }

    public int getIntParameter(String name)
    {
        int index = -1;

        if ((index = this.findParameterByName(name)) < 0)
        {
            throw new RuntimeException("Unknown parameter '" + name + "'");
        }

        return Integer.parseInt(this.parameters.getJSONArray("parameters")
                .getJSONObject(index).getString("value"));
    }

    public double getFloatParameter(String name)
    {
        int index = -1;

        if ((index = this.findParameterByName(name)) < 0)
        {
            throw new RuntimeException("Unknown parameter '" + name + "'");
        }

        return Double.parseDouble(this.parameters.getJSONArray("parameters")
                .getJSONObject(index).getString("value"));
    }

    public boolean getBoolParameter(String name)
    {
        int index = -1;

        if ((index = this.findParameterByName(name)) < 0)
        {
            throw new RuntimeException("Unknown parameter '" + name + "'");
        }

        return Boolean.parseBoolean(this.parameters.getJSONArray("parameters")
                .getJSONObject(index).getString("value"));
    }

    public String getStringParameter(String name)
    {
        int index = -1;

        if ((index = this.findParameterByName(name)) < 0)
        {
            throw new RuntimeException("Unknown parameter '" + name + "'");
        }

        return this.parameters.getJSONArray("parameters").getJSONObject(index)
                .getString("value");
    }

    public JSONObject getJsonParameter(String name)
    {
        int index = -1;

        if ((index = this.findParameterByName(name)) < 0)
        {
            throw new RuntimeException("Unknown parameter '" + name + "'");
        }

        return new JSONObject(this.parameters.getJSONArray("parameters")
                .getJSONObject(index).getString("value"));
    }
}
